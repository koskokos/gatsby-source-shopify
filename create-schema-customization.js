"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createSchemaCustomization = void 0;
const collection_type_1 = require("./type-builders/collection-type");
const common_type_1 = require("./type-builders/common-type");
const location_type_1 = require("./type-builders/location-type");
const media_type_1 = require("./type-builders/media-type");
const metafield_type_1 = require("./type-builders/metafield-type");
const order_type_1 = require("./type-builders/order-type");
const product_type_1 = require("./type-builders/product-type");
const product_variant_type_1 = require("./type-builders/product-variant-type");
function createSchemaCustomization(gatsbyApi, pluginOptions) {
    const { actions } = gatsbyApi;
    const { downloadImages, shopifyConnections: connections, typePrefix, } = pluginOptions;
    const prefix = `${typePrefix}Shopify`;
    const typeDefs = [
        (0, common_type_1.commonTypeBuilder)(prefix),
        (0, media_type_1.mediaTypeBuilder)(prefix),
        (0, metafield_type_1.metafieldTypeBuilder)(prefix),
        (0, product_type_1.productTypeBuilder)(prefix),
        (0, product_variant_type_1.productVariantTypeBuilder)(prefix),
    ];
    if (connections.includes(`collections`)) {
        typeDefs.push((0, collection_type_1.collectionTypeBuilder)(prefix));
    }
    if (connections.includes(`locations`)) {
        typeDefs.push((0, location_type_1.locationTypeBuilder)(prefix));
    }
    if (connections.includes(`orders`)) {
        typeDefs.push((0, order_type_1.orderTypeBuilder)(prefix));
    }
    if (downloadImages) {
        typeDefs.push(`
      extend type ${prefix}Image {
        localFile: File @link(from: "localFile___NODE", by: "id")
      }
    `);
    }
    actions.createTypes(typeDefs);
}
exports.createSchemaCustomization = createSchemaCustomization;
//# sourceMappingURL=create-schema-customization.js.map