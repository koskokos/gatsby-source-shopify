"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createRestClient = exports.createGraphqlClient = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
const errors_1 = require("./errors");
const MAX_BACKOFF_MILLISECONDS = 60000;
// TODO Update logic for handling errors (500 specifically) and update the
// "hacky" code in createRestClient
function createGraphqlClient(options) {
    const url = `https://${options.storeUrl}/admin/api/2021-07/graphql.json`;
    async function graphqlFetch(query, variables, retries = 0) {
        const response = await (0, node_fetch_1.default)(url, {
            method: `POST`,
            headers: {
                "Content-Type": `application/json`,
                "X-Shopify-Access-Token": options.password,
            },
            body: JSON.stringify({
                query,
                variables,
            }),
        });
        if (!response.ok) {
            const waitTime = 2 ** (retries + 1) + 500;
            if (response.status >= 500 && waitTime < MAX_BACKOFF_MILLISECONDS) {
                await new Promise(resolve => setTimeout(resolve, waitTime));
                return graphqlFetch(query, variables, retries + 1);
            }
            throw new errors_1.HttpError(response);
        }
        const json = await response.json();
        return json.data;
    }
    return { request: graphqlFetch };
}
exports.createGraphqlClient = createGraphqlClient;
function createRestClient(options) {
    const baseUrl = `https://${options.storeUrl}/admin/api/2021-01`;
    async function shopifyFetch(path, fetchOptions = {
        headers: {
            "X-Shopify-Access-Token": options.password,
        },
    }, retries = 3) {
        /**
         * This is kind of a hack, but...
         *
         * We do this because although callers will use a relative path,
         * some responses might have pagination links that get fed back
         * to shopifyFetch to retrieve the next page. Those links are
         * absolute URLs so we account for both, but not in a very robust
         * fashion.
         */
        const url = path.includes(options.storeUrl) ? path : `${baseUrl}${path}`;
        const resp = await (0, node_fetch_1.default)(url, fetchOptions);
        if (!resp.ok && retries > 0 && resp.status === 429) {
            // rate limit
            const retryAfter = parseFloat(resp.headers.get(`Retry-After`) || ``);
            await new Promise(resolve => setTimeout(resolve, retryAfter));
            return shopifyFetch(path, fetchOptions, retries - 1);
        }
        return resp;
    }
    return {
        request: async (path) => shopifyFetch(path),
    };
}
exports.createRestClient = createRestClient;
//# sourceMappingURL=clients.js.map