"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sourceNodes = void 0;
const update_cache_1 = require("./update-cache");
const create_operations_1 = require("./create-operations");
const source_from_operation_1 = require("./source-from-operation");
const helpers_1 = require("./helpers");
async function sourceNodes(gatsbyApi, pluginOptions) {
    const { shopifyConnections: connections } = pluginOptions;
    gatsbyApi.reporter.info(`Running ${(0, helpers_1.isPriorityBuild)(pluginOptions) ? `` : `non-`}priority queries`);
    const currentBuildTime = Date.now();
    const lastBuildTime = (0, helpers_1.getLastBuildTime)(gatsbyApi, pluginOptions);
    const { productsOperation, productVariantsOperation, ordersOperation, collectionsOperation, locationsOperation, finishLastOperation, completedOperation, cancelOperationInProgress, } = (0, create_operations_1.createOperations)(gatsbyApi, pluginOptions, lastBuildTime);
    const sourceFromOperation = (0, source_from_operation_1.makeSourceFromOperation)(finishLastOperation, completedOperation, cancelOperationInProgress, gatsbyApi, pluginOptions, lastBuildTime);
    const operations = [productsOperation, productVariantsOperation];
    if (connections.includes(`orders`)) {
        operations.push(ordersOperation);
    }
    if (connections.includes(`collections`)) {
        operations.push(collectionsOperation);
    }
    if (connections.includes(`locations`)) {
        operations.push(locationsOperation);
    }
    for (const op of operations) {
        await sourceFromOperation(op);
    }
    if (lastBuildTime) {
        await (0, update_cache_1.updateCache)(gatsbyApi, pluginOptions, lastBuildTime);
    }
    (0, helpers_1.setLastBuildTime)(gatsbyApi, pluginOptions, currentBuildTime);
}
exports.sourceNodes = sourceNodes;
//# sourceMappingURL=source-nodes.js.map