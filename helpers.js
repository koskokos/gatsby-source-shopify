"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseImageExtension = exports.processShopifyImages = exports.decorateBulkObject = exports.parseShopifyId = exports.isShopifyId = exports.isPriorityBuild = exports.setLastBuildTime = exports.getLastBuildTime = exports.getPluginStatus = exports.createNodeId = void 0;
const gatsby_source_filesystem_1 = require("gatsby-source-filesystem");
const shopify_types_1 = require("./shopify-types");
const pattern = /^gid:\/\/shopify\/(\w+)\/(.+)$/;
function createNodeId(shopifyId, gatsbyApi, { typePrefix }) {
    return gatsbyApi.createNodeId(`${typePrefix}${shopifyId}`);
}
exports.createNodeId = createNodeId;
function getPluginStatus(gatsbyApi) {
    var _a;
    return (((_a = gatsbyApi.store.getState().status.plugins) === null || _a === void 0 ? void 0 : _a[`gatsby-source-shopify`]) || {});
}
exports.getPluginStatus = getPluginStatus;
function getLastBuildTime(gatsbyApi, pluginOptions) {
    const { typePrefix } = pluginOptions;
    const status = getPluginStatus(gatsbyApi);
    return (status[`lastBuildTime${typePrefix}`] &&
        new Date(status[`lastBuildTime${typePrefix}`]));
}
exports.getLastBuildTime = getLastBuildTime;
function setLastBuildTime(gatsbyApi, pluginOptions, currentBuildTime) {
    const { typePrefix } = pluginOptions;
    const status = getPluginStatus(gatsbyApi);
    gatsbyApi.actions.setPluginStatus(Object.assign(Object.assign({}, status), { [`lastBuildTime${typePrefix}`]: currentBuildTime }));
}
exports.setLastBuildTime = setLastBuildTime;
function isPriorityBuild(pluginOptions) {
    const { CI, GATSBY_CLOUD, GATSBY_IS_PR_BUILD, NETLIFY, CONTEXT } = process.env;
    const isGatsbyCloudPriorityBuild = CI === `true` && GATSBY_CLOUD === `true` && GATSBY_IS_PR_BUILD !== `true`;
    const isNetlifyPriorityBuild = CI === `true` && NETLIFY === `true` && CONTEXT === `production`;
    return pluginOptions.prioritize !== undefined
        ? pluginOptions.prioritize
        : isGatsbyCloudPriorityBuild || isNetlifyPriorityBuild;
}
exports.isPriorityBuild = isPriorityBuild;
function isShopifyId(shopifyId) {
    return pattern.test(shopifyId);
}
exports.isShopifyId = isShopifyId;
function parseShopifyId(shopifyId) {
    return shopifyId.match(pattern) || [];
}
exports.parseShopifyId = parseShopifyId;
function decorateBulkObject(input) {
    if (input && typeof input === `object`) {
        if (Array.isArray(input)) {
            return input.map(decorateBulkObject);
        }
        const obj = Object.assign({}, input);
        for (const key of Object.keys(obj)) {
            obj[key] = decorateBulkObject(obj[key]);
        }
        // We must convert ID to ShopifyID so that it doesn't collide with Gatsby's internal ID
        if (obj.id) {
            obj.shopifyId = obj.id;
            delete obj.id;
        }
        return obj;
    }
    return input;
}
exports.decorateBulkObject = decorateBulkObject;
async function processShopifyImages({ actions: { createNode }, createNodeId, cache }, node) {
    const type = parseShopifyId(node.shopifyId)[1];
    const imageFields = shopify_types_1.shopifyTypes[type].imageFields;
    if (imageFields) {
        for (const fieldPath of imageFields) {
            const image = fieldPath
                .split(`.`)
                .reduce((acc, value) => acc[value], node);
            if (image && parseImageExtension(image.originalSrc) !== `gif`) {
                const fileNode = await (0, gatsby_source_filesystem_1.createRemoteFileNode)({
                    url: image.originalSrc,
                    cache,
                    createNode,
                    createNodeId,
                    parentNodeId: node.id,
                });
                image.localFile___NODE = fileNode.id;
            }
        }
    }
}
exports.processShopifyImages = processShopifyImages;
function parseImageExtension(url) {
    const basename = url.split(`?`)[0];
    const dot = basename.lastIndexOf(`.`);
    if (dot !== -1) {
        return basename.slice(dot + 1);
    }
    else {
        throw new Error(`Could not parse file extension from Shopify image URL: ${url}`);
    }
}
exports.parseImageExtension = parseImageExtension;
//# sourceMappingURL=helpers.js.map