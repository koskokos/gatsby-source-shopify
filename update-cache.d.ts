import { SourceNodesArgs } from "gatsby";
export declare function updateCache(gatsbyApi: SourceNodesArgs, pluginOptions: IShopifyPluginOptions, lastBuildTime: Date): Promise<void>;
