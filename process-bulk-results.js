"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processBulkResults = void 0;
const shopify_types_1 = require("./shopify-types");
const helpers_1 = require("./helpers");
function isNode(result) {
    const keys = Object.keys(result);
    return Boolean(result.shopifyId &&
        !(keys.length === 2 && result.__parentId && result.shopifyId));
}
async function processBulkResults(gatsbyApi, pluginOptions, results) {
    let nodeCount = 0;
    const promises = [];
    const children = {};
    for (let i = results.length - 1; i >= 0; i -= 1) {
        const result = (0, helpers_1.decorateBulkObject)(results[i]);
        /**
         * @todo detect the following different as JSON.stringify order is not deterministic
         */
        const resultIsNode = isNode(result);
        if (resultIsNode) {
            const type = (0, helpers_1.parseShopifyId)(result.shopifyId)[1];
            const imageFields = shopify_types_1.shopifyTypes[type].imageFields;
            const referenceFields = shopify_types_1.shopifyTypes[type].referenceFields;
            // Only increment for parent nodes
            if (!result.__parentId)
                nodeCount++;
            // Attach reference fields
            if (referenceFields) {
                for (const referenceField of referenceFields) {
                    result[referenceField] = [];
                }
            }
            // Attach children references / objects
            if (children[result.shopifyId]) {
                for (const child of children[result.shopifyId]) {
                    const childType = typeof child === `string`
                        ? (0, helpers_1.parseShopifyId)(child)[1]
                        : child.__typename;
                    const field = shopify_types_1.shopifyTypes[childType].key;
                    if (field) {
                        result[field] = [
                            typeof child === `string`
                                ? (0, helpers_1.createNodeId)(child, gatsbyApi, pluginOptions)
                                : child,
                            ...(result[field] || []),
                        ];
                    }
                    else {
                        throw new Error(`There is no shopifyType for child type: ${childType}`);
                    }
                }
            }
            if (!(0, helpers_1.isShopifyId)(result.shopifyId)) {
                throw new Error(`Expected an ID in the format gid://shopify/<typename>/<id>`);
            }
            const node = Object.assign(Object.assign({}, result), { id: (0, helpers_1.createNodeId)(result.shopifyId, gatsbyApi, pluginOptions), internal: {
                    type: `${pluginOptions.typePrefix}Shopify${type}`,
                    contentDigest: gatsbyApi.createContentDigest(result),
                } });
            if (pluginOptions.downloadImages && imageFields) {
                promises.push((0, helpers_1.processShopifyImages)(gatsbyApi, node).then(() => gatsbyApi.actions.createNode(node)));
            }
            else {
                gatsbyApi.actions.createNode(node);
            }
        }
        // If the object is a child store the reference
        if (result.__parentId) {
            children[result.__parentId] = [
                ...(children[result.__parentId] || []),
                result.shopifyId || result,
            ];
        }
    }
    await Promise.all(promises);
    return nodeCount;
}
exports.processBulkResults = processBulkResults;
//# sourceMappingURL=process-bulk-results.js.map