import { IUrlBuilderArgs, IGatsbyImageData } from "gatsby-plugin-image";
export declare function urlBuilder({ width, height, baseUrl, format, }: IUrlBuilderArgs<unknown>): string;
export declare function getShopifyImage({ image, ...args }: IGetShopifyImageArgs): IGatsbyImageData;
