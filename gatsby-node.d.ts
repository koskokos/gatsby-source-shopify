import type { GatsbyNode } from "gatsby";
export { sourceNodes } from "./source-nodes";
export { createResolvers } from "./create-resolvers";
export { pluginOptionsSchema } from "./plugin-options-schema";
export { createSchemaCustomization } from "./create-schema-customization";
export declare const onPluginInit: GatsbyNode["onPluginInit"];
