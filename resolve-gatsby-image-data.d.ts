import { GatsbyCache } from "gatsby";
import { IGatsbyImageFieldArgs } from "gatsby-plugin-image/graphql-utils";
import { IGatsbyImageData } from "gatsby-plugin-image";
export declare function makeResolveGatsbyImageData(cache: GatsbyCache): (image: IShopifyImage, { formats, layout, ...remainingOptions }: IGatsbyImageFieldArgs) => Promise<IGatsbyImageData | null>;
