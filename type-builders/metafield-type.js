"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.metafieldTypeBuilder = void 0;
function metafieldTypeBuilder(prefix) {
    return `
      type ${prefix}Metafield implements Node @dontInfer {
        createdAt: Date! @dateformat
        description: String
        id: ID!
        key: String!
        legacyResourceId: String!
        namespace: String!
        ownerType: ${prefix}MetafieldOwnerType!
        shopifyId: String!
        type: String!
        updatedAt: Date! @dateformat
        value: String!
        valueType: String! @deprecated(reason: "\`valueType\` is deprecated and replaced by \`type\` in API version 2021-07.")
      }

      enum ${prefix}MetafieldOwnerType {
        ARTICLE
        BLOG
        COLLECTION
        CUSTOMER
        DRAFTORDER
        ORDER
        PAGE
        PRODUCT
        PRODUCTIMAGE
        PRODUCTVARIANT
        SHOP
      }
    `;
}
exports.metafieldTypeBuilder = metafieldTypeBuilder;
//# sourceMappingURL=metafield-type.js.map