"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.productVariantTypeBuilder = void 0;
function productVariantTypeBuilder(prefix) {
    return `
      type ${prefix}ProductVariant implements Node @dontInfer {
        availableForSale: Boolean!
        barcode: String
        compareAtPrice: Float
        createdAt: Date! @dateformat
        displayName: String!
        id: ID!
        image: ${prefix}Image
        inventoryPolicy: ${prefix}ProductVariantInventoryPolicy!
        inventoryQuantity: Int
        legacyResourceId: String!
        media: [${prefix}Media!]! @link(from: "media___NODE", by: "id")
        metafield(namespace: String! key: String!): ${prefix}Metafield
        metafields: [${prefix}Metafield!]! @link(from: "metafields___NODE", by: "id")
        position: Int!
        presentmentPrices: [${prefix}ProductVariantPricePair!]!
        price: Float!
        contextualPricingUA: ${prefix}ProductVariantContextualPrice!
        contextualPricingUS: ${prefix}ProductVariantContextualPrice!
        product: ${prefix}Product! @link(from: "product.shopifyId", by: "shopifyId")
        requiresShipping: Boolean! @deprecated(reason: "Use \`InventoryItem.requiresShipping\` instead.")
        selectedOptions: [${prefix}SelectedOption!]!
        sellingPlanGroupCount: Int!
        sku: String
        shopifyId: String!
        storefrontId: String!
        taxCode: String
        taxable: Boolean!
        title: String!
        updatedAt: Date! @dateformat
        weight: Float
        weightUnit: ${prefix}WeightUnit!
      }

      enum ${prefix}ProductVariantInventoryPolicy {
        DENY
        CONTINUE
      }

      type ${prefix}ProductVariantPricePair {
        compareAtPrice: ${prefix}MoneyV2
        price: ${prefix}MoneyV2!
      }

      type ${prefix}ProductVariantContextualPrice {
        price: ${prefix}MoneyV2!
      }

      type ${prefix}SelectedOption {
        name: String!
        value: String!
      }
    `;
}
exports.productVariantTypeBuilder = productVariantTypeBuilder;
//# sourceMappingURL=product-variant-type.js.map
