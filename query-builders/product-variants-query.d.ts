import { BulkQuery } from "./bulk-query";
export declare class ProductVariantsQuery extends BulkQuery {
    query(date?: Date): string;
}
