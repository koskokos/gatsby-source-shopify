export declare abstract class BulkQuery {
    pluginOptions: IShopifyPluginOptions;
    constructor(pluginOptions: IShopifyPluginOptions);
    abstract query(date?: Date): string;
    protected bulkOperationQuery(query: string): string;
}
