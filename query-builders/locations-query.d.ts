import { BulkQuery } from "./bulk-query";
export declare class LocationsQuery extends BulkQuery {
    query(date?: Date): string;
}
