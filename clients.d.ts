export declare function createGraphqlClient(options: IShopifyPluginOptions): IGraphQLClient;
export declare function createRestClient(options: IShopifyPluginOptions): IRestClient;
