import { Response } from "node-fetch";
export declare const pluginErrorCodes: {
    bulkOperationFailed: string;
    unknownSourcingFailure: string;
    unknownApiError: string;
    apiConflict: string;
};
export declare class OperationError extends Error {
    node: IBulkOperationNode;
    constructor(node: IBulkOperationNode);
}
export declare class HttpError extends Error {
    response: Response;
    constructor(response: Response);
}
