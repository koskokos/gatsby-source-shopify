"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eventsApi = void 0;
const clients_1 = require("./clients");
function eventsApi(options) {
    const restClient = (0, clients_1.createRestClient)(options);
    return {
        async fetchDestroyEventsSince(date) {
            let resp = await restClient.request(`/events.json?limit=250&verb=destroy&created_at_min=${date.toISOString()}`);
            const { events } = await resp.json();
            let gatherPaginatedEvents = true;
            while (gatherPaginatedEvents) {
                const paginationInfo = resp.headers.get(`link`);
                if (!paginationInfo) {
                    gatherPaginatedEvents = false;
                    break;
                }
                const pageLinks = paginationInfo.split(`,`).map((pageData) => {
                    const [, url, rel] = pageData.match(/<(.*)>; rel="(.*)"/) || [];
                    return {
                        url,
                        rel,
                    };
                });
                const nextPage = pageLinks.find(l => l.rel === `next`);
                if (nextPage) {
                    resp = await restClient.request(nextPage.url);
                    const { events: nextEvents } = await resp.json();
                    events.push(...nextEvents);
                }
                else {
                    gatherPaginatedEvents = false;
                    break;
                }
            }
            return events;
        },
    };
}
exports.eventsApi = eventsApi;
//# sourceMappingURL=events.js.map