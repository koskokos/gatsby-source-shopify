import { CreateSchemaCustomizationArgs } from "../../gatsby";
export declare function createSchemaCustomization(gatsbyApi: CreateSchemaCustomizationArgs, pluginOptions: IShopifyPluginOptions): void;
