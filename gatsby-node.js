"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onPluginInit = exports.createSchemaCustomization = exports.pluginOptionsSchema = exports.createResolvers = exports.sourceNodes = void 0;
const error_map_1 = require("./error-map");
var source_nodes_1 = require("./source-nodes");
Object.defineProperty(exports, "sourceNodes", { enumerable: true, get: function () { return source_nodes_1.sourceNodes; } });
var create_resolvers_1 = require("./create-resolvers");
Object.defineProperty(exports, "createResolvers", { enumerable: true, get: function () { return create_resolvers_1.createResolvers; } });
var plugin_options_schema_1 = require("./plugin-options-schema");
Object.defineProperty(exports, "pluginOptionsSchema", { enumerable: true, get: function () { return plugin_options_schema_1.pluginOptionsSchema; } });
var create_schema_customization_1 = require("./create-schema-customization");
Object.defineProperty(exports, "createSchemaCustomization", { enumerable: true, get: function () { return create_schema_customization_1.createSchemaCustomization; } });
const onPluginInit = ({ reporter, }) => {
    reporter.setErrorMap(error_map_1.ERROR_MAP);
};
exports.onPluginInit = onPluginInit;
//# sourceMappingURL=gatsby-node.js.map