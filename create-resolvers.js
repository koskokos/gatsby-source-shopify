"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createResolvers = void 0;
const graphql_utils_1 = require("gatsby-plugin-image/graphql-utils");
const shopify_types_1 = require("./shopify-types");
const resolve_gatsby_image_data_1 = require("./resolve-gatsby-image-data");
function createResolvers(gatsbyApi, pluginOptions) {
    var _a;
    const { createResolvers, cache } = gatsbyApi;
    const { downloadImages, typePrefix, shopifyConnections: connections, } = pluginOptions;
    if (!downloadImages) {
        createResolvers({
            [`${typePrefix}ShopifyImage`]: {
                gatsbyImageData: Object.assign(Object.assign({}, (0, graphql_utils_1.getGatsbyImageFieldConfig)((0, resolve_gatsby_image_data_1.makeResolveGatsbyImageData)(cache))), { type: `JSON` }),
            },
        });
    }
    // Attach the metafield resolver to all types with a metafields field
    for (const [type, value] of Object.entries(shopify_types_1.shopifyTypes)) {
        if ((_a = value.coupledNodeFields) === null || _a === void 0 ? void 0 : _a.includes(`metafields___NODE`)) {
            // Only include the resolver if the type is included in the build
            if (!value.optionalKey || connections.includes(value.optionalKey)) {
                createResolvers({
                    [`${typePrefix}Shopify${type}`]: {
                        metafield: {
                            resolve: async (source, args, context) => context.nodeModel.findOne({
                                type: `${typePrefix}ShopifyMetafield`,
                                query: {
                                    filter: {
                                        key: { eq: args.key },
                                        namespace: { eq: args.namespace },
                                        id: { in: source.metafields___NODE },
                                    },
                                },
                            }),
                        },
                    },
                });
            }
        }
    }
}
exports.createResolvers = createResolvers;
//# sourceMappingURL=create-resolvers.js.map