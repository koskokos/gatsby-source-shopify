import { SourceNodesArgs } from "gatsby";
interface IOperations {
    productsOperation: IShopifyBulkOperation;
    productVariantsOperation: IShopifyBulkOperation;
    ordersOperation: IShopifyBulkOperation;
    collectionsOperation: IShopifyBulkOperation;
    locationsOperation: IShopifyBulkOperation;
    cancelOperationInProgress: () => Promise<void>;
    cancelOperation: (id: string) => Promise<IBulkOperationCancelResponse>;
    finishLastOperation: () => Promise<void>;
    completedOperation: (operationId: string, interval?: number) => Promise<{
        node: IBulkOperationNode;
    }>;
}
export declare function createOperationObject(graphqlClient: IGraphQLClient, operationQuery: string, name: string): IShopifyBulkOperation;
export declare function createOperations(gatsbyApi: SourceNodesArgs, pluginOptions: IShopifyPluginOptions, lastBuildTime?: Date): IOperations;
export {};
