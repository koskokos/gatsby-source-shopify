import { SourceNodesArgs } from "gatsby";
export declare function makeSourceFromOperation(finishLastOperation: () => Promise<void>, completedOperation: (id: string) => Promise<{
    node: IBulkOperationNode;
}>, cancelOperationInProgress: () => Promise<void>, gatsbyApi: SourceNodesArgs, pluginOptions: IShopifyPluginOptions, lastBuildTime?: Date): (op: IShopifyBulkOperation) => Promise<void>;
