interface IShopifyTypes {
    [key: string]: {
        key?: string;
        coupled: boolean;
        optionalKey?: string;
        imageFields?: Array<string>;
        referenceFields?: Array<string>;
        coupledNodeFields?: Array<string>;
    };
}
export declare const shopifyTypes: IShopifyTypes;
export {};
