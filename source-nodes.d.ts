import { SourceNodesArgs } from "gatsby";
export declare function sourceNodes(gatsbyApi: SourceNodesArgs, pluginOptions: IShopifyPluginOptions): Promise<void>;
