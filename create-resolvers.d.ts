import { CreateResolversArgs } from "gatsby";
export declare function createResolvers(gatsbyApi: CreateResolversArgs, pluginOptions: IShopifyPluginOptions): void;
