import { SourceNodesArgs } from "gatsby";
export declare function processBulkResults(gatsbyApi: SourceNodesArgs, pluginOptions: IShopifyPluginOptions, results: BulkResults): Promise<number>;
